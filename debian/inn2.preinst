#!/bin/sh -e

convert_from_non_lfs() {
  # is this the inn2 or inn2-lfs package?
  if [ "$DPKG_MAINTSCRIPT_PACKAGE" != 'inn2' ]; then return 1; fi

  if [ "$1" = 'upgrade' ]; then
    # only consider upgrades from releases older than 2.5.4-1
    if dpkg --compare-versions $2 ge 2.5.4-1; then return 1; fi

    # did this architecture have a non-LFS inn2 package?
    case "$DPKG_MAINTSCRIPT_ARCH" in
      alpha|arm|hppa|i386|mips|mipsel|powerpc|s390|sparc) : ;;
      *) return 1 ;;
    esac
  elif [ "$1" = 'install' ]; then
    # if we are replacing inn2-lfs then prevent it from deleting the spool
    local inn2_lfs_postrm=$(dpkg-query --control-path inn2-lfs postrm 2> /dev/null || true)
    if [ "$inn2_lfs_postrm" ]; then
      perl -i -pe 's/purge/purge-DISABLED-BY-inn2/' "$inn2_lfs_postrm"
    fi
    return 1
  else
    return 1
  fi

  # has the administrator agreed to continue?
  if [ -e /etc/news/convert-inn-data ]; then
    rm /etc/news/convert-inn-data
    return 0
  fi

  # This will be relevant for only a limited number of users, so I would
  # rather not introduce debconf just for asking the question. --Md
  cat <<END

You are trying to upgrade inn2 on a 32-bit system where an old inn2 package
without Large File Support is currently installed.

Since INN 2.5.4, Debian has stopped providing a 32-bit inn2 package and a
LFS-enabled inn2-lfs package and now only this LFS-enabled inn2 package is
supported.

This will require rebuilding the history index and the overview database,
but the postinst script will attempt to do it for you.

BEWARE: the timecaf spool format is not compatible between 32 and 64 bit
systems and no conversion tool is available.
A possible solution may involve manually feeding the articles to the new
server by using the old innxmit binary in a chroot environment containing
the old spool, history and /etc/news/.

Please create an empty /etc/news/convert-inn-data file and then try again
upgrading inn2 if you want to proceed.

END

  # fail to stop the installation
  exit 1
}

# this should be checked before everything else in the script
if convert_from_non_lfs "$@"; then
  echo "Will rebuild the history and overview databases."
  : > /var/lib/news/must-rebuild-history-index
  : > /var/lib/news/must-rebuild-overview
fi

# #690128: if the old MOTD file has been amended by the admin from default,
# then copy it to the new non-conffile nnrpd MOTD file.
# If not then remove the old MOTD conffile, being sure to cater for rollback.
dpkg-maintscript-helper rm_conffile /etc/news/motd.news 2.5.3-1~ -- "$@"
dpkg-maintscript-helper rm_conffile /etc/news/radius.conf 2.6.1-3~ -- "$@"

#DEBHELPER#

exit 0
